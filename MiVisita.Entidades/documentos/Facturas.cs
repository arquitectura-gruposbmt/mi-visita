﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MiVisita.Entidades.documentos
{
    public class Facturas
    {
        public int id_factura { get; set; }
        [Required(ErrorMessage = "El nombre de la factura es requerido.")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "El nombre de la factura no debe contener más de 100 caracteres ni menos de 3.")]
        public string nombre { get; set; }
        [Required(ErrorMessage = "El directorio de la factura es requerido.")]
        [StringLength(256)]
        public string path_factura { get; set; }
        [Required(ErrorMessage = "El id del usuario es requerido.")]
        public int id_usuario { get; set; }
        [Required]
        public DateTime fecha { get; set; }
    }
}
