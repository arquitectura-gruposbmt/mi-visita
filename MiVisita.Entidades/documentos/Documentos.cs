﻿using System;
using System.ComponentModel.DataAnnotations;
namespace MiVisita.Entidades.documentos
{
    public class Documentos
    {
        public int id_doc { get; set; }
        [Required(ErrorMessage = "El id del fraccionamiento es requerido.")]
        public int id_fracc { get; set; }
        [Required(ErrorMessage = "El nombre del cocumento es requerido.")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "El nombre del documento no debe contener más de 100 caracteres ni menos de 3.")]
        public string nombre { get; set; }
        [Required(ErrorMessage = "El directorio del documento es requerido.")]
        [StringLength(256)]
        public string path_doc { get; set; }
        [Required]
        public DateTime fecha { get; set; }
    }
}
