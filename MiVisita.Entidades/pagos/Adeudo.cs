﻿using System;
using System.ComponentModel.DataAnnotations;
namespace MiVisita.Entidades.pagos
{
    public class Adeudo
    {
        public int id_adeudo { get; set; }
        [Required(ErrorMessage = "El id del concepto es requerido.")]
        public int id_concepto { get; set; }
        [Required(ErrorMessage = "El id del fraccionamiento es requerido.")]
        public int id_fracc { get; set; }
        [Required(ErrorMessage = "El id del usuario es requerido.")]
        public int id_usuario { get; set; }
        [Required]
        public DateTime fecha { get; set; }
        [Required(ErrorMessage = "La cantidad es requerida.")]
        public double cantidad { get; set; }
        public bool estatus { get; set; }
    }
}
