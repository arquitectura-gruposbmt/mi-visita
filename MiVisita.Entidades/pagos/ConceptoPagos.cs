﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MiVisita.Entidades.pagos
{
    public class ConceptoPagos
    {
        public int id_concepto { get; set; }
        [Required(ErrorMessage = "El id del usuario administrador es requerido.")]
        public int id_usuario { get; set; }
        [Required(ErrorMessage = "El id del fraccionamiento es requerido.")]
        public int id_fracc { get; set; }
        [Required(ErrorMessage = "El nombre del concepto es requerido.")]
        [StringLength(100, ErrorMessage = "El nombre no debe contener más de 100 caracteres.")]
        public string nombre { get; set; }
        [StringLength(256, ErrorMessage = "La descripción no debe contener más de 256 caracteres.")]
        public string descripcion { get; set; }
        [Required(ErrorMessage = "La fecha de creación del concepto es requerida.")]
        public DateTime fecha_creacion { get; set; }
        public bool estatus { get; set; }
    }
}
