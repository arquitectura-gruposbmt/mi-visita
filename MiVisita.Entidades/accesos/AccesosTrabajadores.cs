﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MiVisita.Entidades.accesos
{
    public class AccesosTrabajadores
    {
        public int id_acceso { get; set; }
        [Required(ErrorMessage = "El id del vigilante es requerido.")]
        public int vigilante { get; set; }
        [Required(ErrorMessage = "El id del trabajador es requerido.")]
        public int id_trabajador { get; set; }
        [Required(ErrorMessage = "El/los id(s) de los residentes son requeridos.")]
        public string residentes { get; set; }
        [Required(ErrorMessage = "La fecha de entrada es requerida.")]
        public DateTime fecha_entrada { get; set; }
        [Required(ErrorMessage = "La fecha de salida es requerida.")]
        public DateTime fecha_salida { get; set; }
        public bool turno { get; set; }
    }
}
