﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MiVisita.Entidades.accesos
{
    public class TurnosVigilantes
    {
        public int id_aceso { get; set; }
        [Required(ErrorMessage = "El id del vigilante es requerido.")]
        public int id_vigilante { get; set; }
        [Required(ErrorMessage = "La fecha de entrada es requerida.")]
        public DateTime fecha_entrada { get; set; }
        [Required(ErrorMessage = "La fecha de salida es requerida.")]
        public DateTime fecha_salida { get; set; }
        public bool turno { get; set; }
    }
}
