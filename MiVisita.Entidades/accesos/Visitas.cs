﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MiVisita.Entidades.accesos
{
    public class Visitas
    {
        public int id_visita { get; set; }
        [Required(ErrorMessage = "El id del vigilante es requerido.")]
        public int vigilante { get; set; }
        [Required(ErrorMessage = "El id del residente es requerido.")]
        public int residente { get; set; }
        [Required(ErrorMessage = "El nombre del visitante es requerido.")]
        [StringLength(50)]
        public string nombre { get; set; }
        [Required(ErrorMessage = "El apellido paterno del visitante es requerido.")]
        [StringLength(20)]
        public string apellido_paterno { get; set; }
        [StringLength(20)]
        public string apellido_materno { get; set; }
        [Required(ErrorMessage = "La fecha de entrada es requerida.")]
        public DateTime fecha_entrada { get; set; }
        [Required(ErrorMessage = "La fecha de salida es requerida.")]
        public DateTime fecha_salida { get; set; }
        [StringLength(10)]
        public string placas { get; set; }
        public bool estatus_visita { get; set; }
    }
}
