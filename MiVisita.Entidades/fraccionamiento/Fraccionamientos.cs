﻿using System.ComponentModel.DataAnnotations;

namespace MiVisita.Entidades.fraccionamiento
{
    public class Fraccionamientos
    {
        public int id_fracc { get; set; }
        [Required(ErrorMessage = "El nombre del fraccionameinto es requerido.")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "El nombre debe ser mayor a 3 y menos a 100 catarcteres.")]
        public string nombre_fracc { get; set; }
        [Required(ErrorMessage = "El tipo del fraccionamiento es requerido.")]
        public int tipo_fracc { get; set; }
        [Required(ErrorMessage = "La cantidad de viviendas es requerida.")]
        public int cantidad_viviendas { get; set; }
        public bool estatus { get; set; }
    }
}
