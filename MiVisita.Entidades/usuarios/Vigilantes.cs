﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MiVisita.Entidades.usuarios
{
    public class Vigilantes
    {
        public int id_vigilante { get; set; }
        [Required(ErrorMessage = "El id del fraccionamiento es requerido.")]
        public int id_fracc { get; set; }
        [Required(ErrorMessage = "El id del usuario administrador es requerido.")]
        public int usuario_admin { get; set; }
        [Required(ErrorMessage = "El nombre del vigilante es requerido.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El nombre del vigilante no debe contener más de 50 caracteres ni menos de 3.")]
        public string nombre { get; set; }
        [Required(ErrorMessage = "El apellido paterno es requerido.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El apellido paterno del vigilante no debe contener más de 20 caracteres ni menos de 3.")]
        public string apellido_paterno { get; set; }
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El apellido materno del vigilante no debe contener más de 20 caracteres ni menos de 3.")]
        public string apellido_materno { get; set; }
        [Required]
        public byte[] password_hash { get; set; }
        [Required]
        public byte[] password_salt { get; set; }
        [Required(ErrorMessage = "La fecha de creación del vigilante es requerida.")]
        public DateTime fecha_creado { get; set; }
        public bool estatus { get; set; }
    }
}
