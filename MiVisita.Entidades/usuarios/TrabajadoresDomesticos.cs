﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MiVisita.Entidades.usuarios
{
    public class TrabajadoresDomesticos
    {
        public int id_trabajador { get; set; }
        [Required(ErrorMessage = "El id del fraccionamiento es requerido.")]
        public int id_fracc { get; set; }
        [Required(ErrorMessage = "El id del usuario administrador es requerido.")]
        public int usuario_admin { get; set; }
        [Required(ErrorMessage = "El id del residente es requerido.")]
        public int empleador { get; set; }
        [Required(ErrorMessage = "El nombre del trabajador es requerido.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El nombre del trabajador no debe contener más de 50 caracteres ni menos de 3.")]
        public string nombre { get; set; }
        [Required(ErrorMessage = "El apellido paterno es requerido.")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "El apellido paterno del trabajador no debe contener más de 20 caracteres nimenos de 3.")]
        public string apellido_paterno { get; set; }
        [StringLength(20, MinimumLength = 3, ErrorMessage = "El apellido materno del trabajador no debe contener más de 20 caracteres ni menos de 3.")]
        public string apellido_materno { get; set; }
        [StringLength(15)]
        public string telefono { get; set; }
        [Required(ErrorMessage = "El correo es requerido.")]
        [StringLength(50)]
        public string email { get; set; }
        [Required]
        public DateTime fecha_creado { get; set; }
        public bool estatus { get; set; }
    }
}
