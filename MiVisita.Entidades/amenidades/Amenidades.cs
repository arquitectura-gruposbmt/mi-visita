﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MiVisita.Entidades.amenidades
{
    public class Amenidades
    {
        public int id_amenidad { get; set; }
        [Required(ErrorMessage = "El id del usuario es requerido.")]
        public int id_usuario { get; set; }
        [Required(ErrorMessage = "El id del fraccionamiento es requerido.")]
        public int id_fracc { get; set; }
        [Required(ErrorMessage = "El nombre de la amenidad es requerido.")]
        [StringLength(100)]
        public string nombre_amenidad { get; set; }
        [StringLength(256)]
        public string descripcion { get; set; }
        [Required(ErrorMessage = "El horario de apertura es requerido.")]
        public DateTime horario_apertura { get; set; }
        [Required(ErrorMessage = "El horario de cierre es requerido.")]
        public DateTime horario_cierre { get; set; }
        public bool estatus { get; set; }

    }
}
