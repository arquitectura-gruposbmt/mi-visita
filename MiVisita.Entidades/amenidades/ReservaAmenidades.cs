﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MiVisita.Entidades.amenidades
{
    public class ReservaAmenidades
    {
        public int id_reserva { get; set; }
        [Required(ErrorMessage = "El id del usuario es requerido.")]
        public int id_usuario { get; set; }
        [Required(ErrorMessage = "El id del fraccionamiento es requerido.")]
        public int id_fracc { get;set; }
        [Required(ErrorMessage = "El id de la amenidad es requerido.")]
        public int id_amenidad { get; set; }
        [Required]
        public DateTime fecha { get; set; }
        public int aprobacion { get; set; }
        [Required(ErrorMessage = "El id del usuario administrador es requerido.")]
        public int usuario_admin { get; set; }
    }
}
