﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.amenidades;

namespace MiVisita.Datos.Mapping.amenidades
{
    public class AmenidadesMap : IEntityTypeConfiguration<Amenidades>
    {
        public void Configure(EntityTypeBuilder<Amenidades> builder)
        {
            builder.ToTable("amenidades")
                .HasKey(c => c.id_amenidad);
        }
    }
}
