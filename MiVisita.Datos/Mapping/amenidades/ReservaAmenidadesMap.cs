﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.amenidades;

namespace MiVisita.Datos.Mapping.amenidades
{
    public class ReservaAmenidadesMap : IEntityTypeConfiguration<ReservaAmenidades>
    {
        public void Configure(EntityTypeBuilder<ReservaAmenidades> builder)
        {
            builder.ToTable("reserva_amenidades")
                .HasKey(c => c.id_reserva);
        }
    }
}
