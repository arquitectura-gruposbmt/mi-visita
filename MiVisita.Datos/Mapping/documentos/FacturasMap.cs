﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.documentos;

namespace MiVisita.Datos.Mapping.documentos
{
    public class FacturasMap : IEntityTypeConfiguration<Facturas>
    {
        public void Configure(EntityTypeBuilder<Facturas> builder)
        {
            builder.ToTable("facturas")
                .HasKey(c => c.id_factura);
        }
    }
}
