﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.documentos;

namespace MiVisita.Datos.Mapping.documentos
{
    public class DocumentosMap : IEntityTypeConfiguration<Documentos>
    {
        public void Configure(EntityTypeBuilder<Documentos> builder)
        {
            builder.ToTable("documentos")
                .HasKey(c => c.id_doc);
        }
    }
}
