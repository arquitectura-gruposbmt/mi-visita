﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.fraccionamiento;

namespace MiVisita.Datos.Mapping.fraccionamientos
{
    public class FraccionamientosMap : IEntityTypeConfiguration<Fraccionamientos>
    {
        public void Configure(EntityTypeBuilder<Fraccionamientos> builder)
        {
            builder.ToTable("fraccionamientos")
                .HasKey(c => c.id_fracc);
        }
    }
}
