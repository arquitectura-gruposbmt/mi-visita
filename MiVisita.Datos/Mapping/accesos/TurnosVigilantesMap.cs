﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.accesos;

namespace MiVisita.Datos.Mapping.accesos
{
    public class TurnosVigilantesMap : IEntityTypeConfiguration<TurnosVigilantes>
    {
        public void Configure(EntityTypeBuilder<TurnosVigilantes> builder)
        {
            builder.ToTable("turnos_vigilantes")
                .HasKey(c => c.id_aceso);
        }
    }
}
