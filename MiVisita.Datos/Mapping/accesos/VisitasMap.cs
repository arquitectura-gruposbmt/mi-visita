﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.accesos;

namespace MiVisita.Datos.Mapping.accesos
{
    public class VisitasMap : IEntityTypeConfiguration<Visitas>
    {
        public void Configure(EntityTypeBuilder<Visitas> builder)
        {
            builder.ToTable("visitas")
                .HasKey(c => c.id_visita);
        }
    }
}
