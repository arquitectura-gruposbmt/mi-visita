﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.accesos;

namespace MiVisita.Datos.Mapping.accesos
{
    public class AccesosTrabajadoresMap : IEntityTypeConfiguration<AccesosTrabajadores>
    {
        public void Configure(EntityTypeBuilder<AccesosTrabajadores> builder)
        {
            builder.ToTable("accesos_trabajadores")
                .HasKey(c => c.id_acceso);
        }
    }
}
