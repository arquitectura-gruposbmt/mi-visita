﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.usuarios;

namespace MiVisita.Datos.Mapping.usuarios
{
    public class VigilantesMap : IEntityTypeConfiguration<Vigilantes>
    {
        public void Configure(EntityTypeBuilder<Vigilantes> builder)
        {
            builder.ToTable("vigilantes")
                .HasKey(c => c.id_vigilante);
        }
    }
}
