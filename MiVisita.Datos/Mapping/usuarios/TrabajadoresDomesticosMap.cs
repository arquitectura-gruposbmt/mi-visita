﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.usuarios;

namespace MiVisita.Datos.Mapping.usuarios
{
    public class TrabajadoresDomesticosMap : IEntityTypeConfiguration<TrabajadoresDomesticos>
    {
        public void Configure(EntityTypeBuilder<TrabajadoresDomesticos> builder)
        {
            builder.ToTable("trabajadores_domesticos")
                .HasKey(c => c.id_trabajador);
        }
    }
}
