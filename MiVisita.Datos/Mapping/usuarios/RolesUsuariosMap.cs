﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.usuarios;

namespace MiVisita.Datos.Mapping.usuarios
{
    public class RolesUsuariosMap : IEntityTypeConfiguration<RolesUsuarios>
    {
        public void Configure(EntityTypeBuilder<RolesUsuarios> builder)
        {
            builder.ToTable("roles_usuarios")
                .HasKey(c => c.id_rol);
        }
    }
}
