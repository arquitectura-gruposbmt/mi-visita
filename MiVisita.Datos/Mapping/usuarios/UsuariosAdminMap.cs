﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.usuarios;

namespace MiVisita.Datos.Mapping.usuarios
{
    public class UsuariosAdminMap : IEntityTypeConfiguration<UsuariosAdmin>
    {
        public void Configure(EntityTypeBuilder<UsuariosAdmin> builder)
        {
            builder.ToTable("usuarios_admin")
                .HasKey(c => c.id_usuario);
        }
    }
}
