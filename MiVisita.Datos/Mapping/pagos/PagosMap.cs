﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.pagos;
using System;

namespace MiVisita.Datos.Mapping.pagos
{
    public class PagosMap : IEntityTypeConfiguration<Pagos>
    {
        public void Configure(EntityTypeBuilder<Pagos> builder)
        {
            builder.ToTable("pagos")
                .HasKey(c => c.id_pago);
        }
    }
}
