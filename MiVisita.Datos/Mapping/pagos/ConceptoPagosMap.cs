﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.pagos;

namespace MiVisita.Datos.Mapping.pagos
{
    public class ConceptoPagosMap : IEntityTypeConfiguration<ConceptoPagos>
    {
        public void Configure(EntityTypeBuilder<ConceptoPagos> builder)
        {
            builder.ToTable("concepto_pagos")
                .HasKey(c => c.id_concepto);
        }
    }
}
