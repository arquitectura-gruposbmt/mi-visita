﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MiVisita.Entidades.pagos;

namespace MiVisita.Datos.Mapping.pagos
{
    public class AdeudoMap : IEntityTypeConfiguration<Adeudo>
    {
        public void Configure(EntityTypeBuilder<Adeudo> builder)
        {
            builder.ToTable("adeudos")
                .HasKey(c => c.id_adeudo);
        }
    }
}
