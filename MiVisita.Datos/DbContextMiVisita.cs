﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using MiVisita.Datos.Mapping.accesos;
using MiVisita.Datos.Mapping.amenidades;
using MiVisita.Datos.Mapping.documentos;
using MiVisita.Datos.Mapping.fraccionamientos;
using MiVisita.Datos.Mapping.pagos;
using MiVisita.Datos.Mapping.usuarios;
using MiVisita.Entidades.accesos;
using MiVisita.Entidades.amenidades;
using MiVisita.Entidades.documentos;
using MiVisita.Entidades.fraccionamiento;
using MiVisita.Entidades.pagos;
using MiVisita.Entidades.usuarios;

namespace MiVisita.Datos
{
    public class DbContextMiVisita : DbContext
    {
        public DbSet<AccesosTrabajadores> AccesosTrabajadores { get; set; }
        public DbSet<TurnosVigilantes> TurnosVigilantes { get; set; }
        public DbSet<Visitas> Visitas { get; set; }
        public DbSet<Amenidades> Amenidades { get; set; }
        public DbSet<ReservaAmenidades> ReservaAmenidades { get; set; }
        public DbSet<Documentos> Documentos { get; set; }
        public DbSet<Facturas> Facturas { get; set; }
        public DbSet<Fraccionamientos> Fraccionamientos { get; set; }
        public DbSet<Adeudo> Adeudp { get; set; }
        public DbSet<ConceptoPagos> ConceptoPagos { get; set; }
        public DbSet<Pagos> Pagos { get; set; }
        public DbSet<RolesUsuarios> RolesUsuarios { get; set; }
        public DbSet<TrabajadoresDomesticos> TrabajadoresDomesticos { get; set; }
        public DbSet<UsuariosAdmin> UsuariosAdmin { get; set; }
        public DbSet<Usuarios> Usuarios { get; set; }
        public DbSet<Vigilantes> Vigilantes { get; set; }


        public DbContextMiVisita(DbContextOptions<DbContextMiVisita> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AccesosTrabajadoresMap());
            modelBuilder.ApplyConfiguration(new TurnosVigilantesMap());
            modelBuilder.ApplyConfiguration(new VisitasMap());
            modelBuilder.ApplyConfiguration(new AmenidadesMap());
            modelBuilder.ApplyConfiguration(new ReservaAmenidadesMap());
            modelBuilder.ApplyConfiguration(new DocumentosMap());
            modelBuilder.ApplyConfiguration(new FacturasMap());
            modelBuilder.ApplyConfiguration(new FraccionamientosMap());
            modelBuilder.ApplyConfiguration(new AdeudoMap());
            modelBuilder.ApplyConfiguration(new ConceptoPagosMap());
            modelBuilder.ApplyConfiguration(new PagosMap());
            modelBuilder.ApplyConfiguration(new RolesUsuariosMap());
            modelBuilder.ApplyConfiguration(new TrabajadoresDomesticosMap());
            modelBuilder.ApplyConfiguration(new UsuariosAdminMap());
            modelBuilder.ApplyConfiguration(new VigilantesMap());
            modelBuilder.ApplyConfiguration(new UsuariosMap());
        }
    }
}
