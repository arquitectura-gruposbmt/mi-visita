import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AccessGuard implements CanActivate, CanLoad {

  private state;
  private route;

  constructor(private authService: AuthService, private router: Router) { }
  
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.state = state;
    this.route = route;
    return this.accessAuthorization(this.route, this.state);
  }

  //canActivate() {
  //  return this.canLoad();
  //}

  canLoad() {
    return true;
  }

  accessAuthorization(route, state) {
    const user = this.authService.getUser();
    var url_controller;
    if (user) {
      if (!this.authService.isTokenExpired()) {
        // check if route is restricted by role
        if (route.data.roles && route.data.roles.indexOf(user.rol) === -1) {
          // role not authorised so redirect to home page
          return false;
        }

        // authorised so return true
        return true;
      } else {
        alert("Su sesión ha expirado, por favor inicie sesión nuevamente.");
        (user.rol == "Administrador") ? url_controller = "UsuariosAdmins" : url_controller = "Usuarios";
        this.authService.logout(url_controller)
          .subscribe(() => {
            this.router.navigate(['/login']);
          });
        return false;
      }
    }
    
    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login']);
    return false;
  }
}
