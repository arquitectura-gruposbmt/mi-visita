import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router, private activatedRoute: ActivatedRoute) { }

  canActivate() {
    //alert(this.authService.isTokenExpired());
    if (this.authService.isTokenExpired() && this.authService.isLoggedIn()) {
      var user = this.authService.getUser();
      var url_controller;
      (user.rol == "Administrador") ? url_controller = "UsuariosAdmins" : url_controller = "Usuarios";
      this.authService.logout(url_controller)
        .subscribe(() => {
          this.router.navigate(['/login']);
        });
    }
    return !this.authService.isLoggedIn();
  }
}
