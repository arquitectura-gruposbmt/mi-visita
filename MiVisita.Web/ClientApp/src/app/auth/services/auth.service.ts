import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { catchError, mapTo, tap } from 'rxjs/operators';
import { Tokens } from '../models/tokens';
//import { jwt_decode } from 'jwt-decode';
//var jwt_decode = require('jwt-decode');
import * as jwt_decode from 'jwt-decode';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly JWT_TOKEN = 'JWT_TOKEN';
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';
  private loggedUser: string;
  private baseUrl: string;

  constructor(private http: HttpClient, @Inject("BASE_URL") BaseUrl: string) { this.baseUrl = BaseUrl; }

  login(user: { email: string, password: string }, name_controller: string): Observable<boolean> {
    return this.http.post<any>(this.baseUrl + "api/" + name_controller + "/Login", user, httpOptions)
      .pipe(
        tap(tokens => {
          this.doLoginUser(user.email, tokens)
        }),
        mapTo(true),
        catchError(error => {
          alert(error.error);
          return of(false);
        }));
  }

  logout(name_controller: string) {
    return this.http.post<any>(this.baseUrl + "api/" + name_controller + "/Logout", {
      'id_usuario': parseInt(this.getUserId()),
      'refreshToken': this.getRefreshToken()
    }, httpOptions).pipe(
      tap(() => this.doLogoutUser()),
      mapTo(true),
      catchError(error => {
        alert(error.error);
        return of(false);
      }));
  }

  refreshToken(name_controller: string) {
    return this.http.post<any>(this.baseUrl + "api/" + name_controller + "/RefreshToken", {
      'id_usuario': parseInt(this.getUserId()),
      'refreshToken': this.getRefreshToken()
    }, httpOptions).pipe(
      tap((tokens: Tokens) => {
        this.storeJwtToken(tokens.jwt);
      }),
      mapTo(true),
      catchError(error => {
        alert(error.error);
        return of(false);
      }));
  }

  isLoggedIn() {
    return !!this.getJwtToken();
  }

  getUser() {
    var token = localStorage.getItem(this.JWT_TOKEN);
    var user;
    (token) ? user = jwt_decode(token) : user = false;
    return user;
  }

  getUserId() {
    var token = localStorage.getItem(this.JWT_TOKEN);
    var user;
    (token) ? user = jwt_decode(token) : user = false;
    return user.id_usuario;
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  getTokenExpirationDate(token: string): Date {
    const decoded = jwt_decode(token);

    if (decoded.exp === undefined) return null;

    const date = new Date(0);
    date.setUTCSeconds(decoded.exp);
    return date;
  }

  isTokenExpired(token?: string): boolean {
    if (!token) token = this.getJwtToken();
    if (!token) return true;

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) return false;
    return !(date.valueOf() > new Date().valueOf());
  }

  private doLoginUser(username: string, tokens: Tokens) {
    this.loggedUser = username;
    this.storeTokens(tokens);
  }

  private doLogoutUser() {
    this.loggedUser = null;
    this.removeTokens();
  }

  private getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  private storeTokens(tokens: Tokens) {
    localStorage.setItem(this.JWT_TOKEN, tokens.jwt);
    localStorage.setItem(this.REFRESH_TOKEN, tokens.refreshToken);
  }

  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
    localStorage.removeItem(this.REFRESH_TOKEN);
  }
}
