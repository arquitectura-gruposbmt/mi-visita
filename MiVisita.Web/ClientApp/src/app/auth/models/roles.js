"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Rol;
(function (Rol) {
    Rol["User"] = "Residente";
    Rol["Admin"] = "Administrador";
    Rol["Vigilante"] = "Vigilante";
})(Rol = exports.Rol || (exports.Rol = {}));
//# sourceMappingURL=roles.js.map