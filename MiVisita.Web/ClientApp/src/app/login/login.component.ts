import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { login } from '../interfaces';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type' : 'application/json'
  })
}

@Component({
  selector: 'app-log-in',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  public list: string[];
  baseUrl: string;
  emailControl = new FormControl('');
  passwordControl = new FormControl('');
  //private currentuserSubject: BehaviorSubject<>;

  constructor(protected http: HttpClient, @Inject("BASE_URL") BaseUrl: string) {
    this.baseUrl = BaseUrl;
  }
  
  public Login() {
    this.http.post<login>(this.baseUrl + 'api/UsuariosAdmins/Login', {
      'email': this.emailControl.value,
      'password': this.passwordControl.value
    }, httpOptions)
      .subscribe(result => {
        console.log(result);
        localStorage.setItem("token", JSON.stringify(result));
      },
        error => console.error(error)
      );
    this.emailControl.setValue('');
    this.passwordControl.setValue('');
  }
}
