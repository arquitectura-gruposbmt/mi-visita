import { Component, OnInit } from "@angular/core";
import { AvisosService } from "./Services/avisos.service";
import { avisos } from "../interfaces";
import { DatePipe, formatDate } from "@angular/common";
import { LoginComponent } from "../login/login.component";
import { AuthService } from "../auth/services/auth.service";

@Component({
  selector: "app-avisos-component",
  templateUrl: "./avisos.component.html",
  styleUrls: ["./avisos.component.css"],
  providers: [DatePipe],
})
export class avisosComponent implements OnInit {
  Avisos;
  Fecha_hoy = formatDate(new Date(), "yyyy-MM-dd", "en");
  Fecha_hoyF;
  User_rol: string;
  Prioridad;
  iD;
  User_IdN: number;
  flag: boolean;
  edit: string = "Crear";

  model: avisos = {
    id: 0,
    mensaje: "",
    Prioridad: 0,
    Fecha_Creacion: this.Fecha_hoy,
    Fecha_Final: null,
    ID_Usuario: 0,
    ID_Fraccionamiento: 1,
  };

  constructor(
    private avisosService: AvisosService,
    private datePipe: DatePipe,
    private user: AuthService
  ) {}

  ngOnInit() {
    this.avisosService.getAvisos().subscribe((data) => {
      this.Avisos = data;

      if (this.user.getUserIdRol() === "Administrador") {
        this.flag = true;
      } else {
        this.flag = false;
      }
    });
  }

  saveNew() {
    this.model.ID_Usuario = Number(this.user.getUserId());

    this.model.Prioridad = Number(this.model.Prioridad);
    console.log(this.model);
    if (this.model.id === 0) {
      this.avisosService
        .addAvisos(this.model)
        .subscribe((response: avisos) => console.log(response));
    } else {
      this.avisosService
        .editAvisos(this.model)
        .subscribe((response: avisos) => console.log(response));
    }

    window.location.reload();
  }

  Edit(Avisos: avisos) {
    this.model = Avisos;
    this.edit = "Modificar";
    window.scroll(0, 0);
  }
  delete(Avisos: avisos) {
    var r = confirm("¿Estás seguro de eliminarlo?");
    if (r == true) {
      this.model = Avisos;
      this.avisosService
        .deleteAvisos(this.model)
        .subscribe((response: avisos) => console.log(response));
      window.location.reload();
    }
  }
  /*saveNew(){
    
    const newAviso = {ID:0,ID_Usuario:1,ID_Fraccionamiento:1,Texto: 'Prueba POST',Fecha_Creacion:this.Fecha_hoyF,Fecha_Final:null,Prioridad:3  }
    this.avisosService.createAvisos(newAviso).subscribe(data=>console.log(data))
  }*/
}
