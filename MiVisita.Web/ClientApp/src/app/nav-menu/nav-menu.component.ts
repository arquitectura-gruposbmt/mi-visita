import { Component } from '@angular/core';
import { AuthService } from '../auth/services/auth.service';
import { stringify } from 'querystring';


@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {

  constructor(private authService: AuthService) {}

  isExpanded = false;

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }

  logout() {
    var user = this.authService.getUser();
    var url_controller;
    (user.rol == "Administrador") ? url_controller = "UsuariosAdmins" : url_controller = "Usuarios";
    this.authService.logout(url_controller)
      .subscribe();
  }
}
