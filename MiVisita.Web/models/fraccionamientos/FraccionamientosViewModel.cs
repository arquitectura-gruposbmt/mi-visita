﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiVisita.Web.models.fraccionamientos
{
    public class FraccionamientosViewModel
    {
        public int id_fracc { get; set; }
        public string nombre_fracc { get; set; }
        public int tipo_fracc { get; set; }
        public int cantidad_viviendas { get; set; }
        public bool estatus { get; set; }
    }
}
