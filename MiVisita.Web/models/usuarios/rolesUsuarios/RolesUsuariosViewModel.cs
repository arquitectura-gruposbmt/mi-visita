﻿
namespace MiVisita.Web.models.usuarios.rolesUsuarios
{
    public class RolesUsuariosViewModel
    {
        public int id_rol { get; set; }
        public int id_fracc { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
    }
}
