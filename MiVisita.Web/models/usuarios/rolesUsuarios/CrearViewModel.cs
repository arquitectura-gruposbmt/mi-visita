﻿using System.ComponentModel.DataAnnotations;

namespace MiVisita.Web.models.usuarios.rolesUsuarios
{
    public class CrearViewModel
    {
        public int id_fracc { get; set; }
        [Required(ErrorMessage = "El nombre es requerido.")]
        [StringLength(20, MinimumLength = 3, ErrorMessage = "El debe tener mas de 3 y menos de 20 catacteres.")]
        public string nombre { get; set; }
        [StringLength(256)]
        public string descripcion { get; set; }
    }
}
