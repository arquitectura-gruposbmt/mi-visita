﻿using System.ComponentModel.DataAnnotations;

namespace MiVisita.Web.models.usuarios.usuarios
{
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string email { get; set; }
        [Required]
        public string password { get; set; }
    }
}
