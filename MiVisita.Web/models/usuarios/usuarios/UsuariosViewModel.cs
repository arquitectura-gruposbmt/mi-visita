﻿using System.ComponentModel.DataAnnotations;

namespace MiVisita.Web.models.usuarios.usuarios
{
    public class UsuariosViewModel
    {
        public int id_usuario { get; set; }
        [Required(ErrorMessage = "El id del fraccionamiento es requerido.")]
        public int id_fracc { get; set; }
        [Required(ErrorMessage = "El id del rol es requerido.")]
        public int rol_usuario { get; set; }
        [Required(ErrorMessage = "El nombre es requerido")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El nombre del usuario no debe contener más de 50 catacteres ni menos de 3.")]
        public string nombre { get; set; }
        [Required(ErrorMessage = "El apellido paterno es requerido.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El apellido paterno del usuario no debe contener más de 50 caracteres ni menos de 3.")]
        public string apellido_paterno { get; set; }
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El apellido materno del usuario no debe contener más de 50 caracteres ni menos de 3.")]
        public string apellido_materno { get; set; }
        [Required]
        public byte[] password_hash { get; set; }
        [StringLength(15)]
        public string telefono { get; set; }
        [Required(ErrorMessage = "El correo es requerido.")]
        [StringLength(50)]
        public string email { get; set; }
        [Required(ErrorMessage = "La dirección es requerida.")]
        [StringLength(256)]
        public string direccion { get; set; }
    }
}
