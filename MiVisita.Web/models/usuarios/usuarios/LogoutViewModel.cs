﻿
namespace MiVisita.Web.models.usuarios.usuarios
{
    public class LogoutViewModel
    {
        public int id_usuario { get; set; }
        public string refreshToken { get; set; }
    }
}
