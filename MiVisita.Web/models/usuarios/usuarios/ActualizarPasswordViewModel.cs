﻿using System.ComponentModel.DataAnnotations;

namespace MiVisita.Web.models.usuarios.usuarios
{
    public class ActualizarPasswordViewModel
    {
        public int id_usuario { get; set; }
        [Required]
        public string password { get; set; }
    }
}
