﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MiVisita.Datos;
using MiVisita.Entidades.fraccionamiento;
using MiVisita.Web.models.fraccionamientos;

namespace MiVisita.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FraccionamientosController : ControllerBase
    {
        private readonly DbContextMiVisita _context;

        public FraccionamientosController(DbContextMiVisita context)
        {
            _context = context;
        }

        // GET: api/Fraccionamientos
        [HttpGet]
        public async Task<IEnumerable<FraccionamientosViewModel>> GetFraccionamientos()
        {
            var fraccionamientos = await _context.Fraccionamientos.ToListAsync();

            return fraccionamientos.Select(f => new FraccionamientosViewModel
            {
                id_fracc = f.id_fracc,
                nombre_fracc = f.nombre_fracc,
                tipo_fracc = f.tipo_fracc,
                cantidad_viviendas = f.cantidad_viviendas,
                estatus = f.estatus
            });
        }

        // GET: api/Fraccionamientos/GetFraccionamiento/5
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<FraccionamientosViewModel>> GetFraccionamiento(int id)
        {
            var fraccionamiento = await _context.Fraccionamientos.FirstOrDefaultAsync(f => f.id_fracc == id);

            return new FraccionamientosViewModel { 
                id_fracc = fraccionamiento.id_fracc,
                nombre_fracc = fraccionamiento.nombre_fracc,
                tipo_fracc = fraccionamiento.tipo_fracc,
                cantidad_viviendas = fraccionamiento.cantidad_viviendas,
                estatus = fraccionamiento.estatus
            };
        }

        // PUT: api/Fraccionamientos/Crear
        [HttpPost("[action]")]
        public async Task<ActionResult<FraccionamientosViewModel>> Crear([FromBody] FraccionamientosViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Fraccionamientos fraccionamiento = new Fraccionamientos
            {
                nombre_fracc = model.nombre_fracc,
                tipo_fracc = model.tipo_fracc,
                cantidad_viviendas = model.cantidad_viviendas,
                estatus = true
            };

            _context.Add(fraccionamiento);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // POST: api/Fraccionamientos/Actualizar
        [HttpPut("[action]")]
        public async Task<ActionResult<FraccionamientosViewModel>> Actualizar([FromBody] FraccionamientosViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.id_fracc <= 0)
            {
                return BadRequest();
            }

            var fraccionamiento = await _context.Fraccionamientos.FirstOrDefaultAsync(f => f.id_fracc == model.id_fracc);

            if(fraccionamiento == null)
            {
                return NotFound("El fraccionamiento que intenta actualizar no existe");
            }

            fraccionamiento.nombre_fracc = model.nombre_fracc;
            fraccionamiento.tipo_fracc = model.tipo_fracc;
            fraccionamiento.cantidad_viviendas = model.cantidad_viviendas;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok("El fraccionamiento se ha actualizado exitosamente.");
        }

        // PUT: api/Fraccionamientos/Activar
        [HttpPut("[action]")]
        public async Task<ActionResult<FraccionamientosViewModel>> Activar([FromBody] FraccionamientosViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.id_fracc <= 0)
            {
                return BadRequest();
            }

            var fraccionamiento = await _context.Fraccionamientos.FirstOrDefaultAsync(u => u.id_fracc == model.id_fracc);

            if (fraccionamiento == null)
            {
                return NotFound("El usuario no existe");
            }

            fraccionamiento.estatus = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // PUT: api/Fraccionamientos/Desactivar
        [HttpPut("[action]")]
        public async Task<ActionResult<FraccionamientosViewModel>> Desactivar([FromBody] FraccionamientosViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.id_fracc <= 0)
            {
                return BadRequest();
            }

            var fraccionamiento = await _context.Fraccionamientos.FirstOrDefaultAsync(u => u.id_fracc == model.id_fracc);

            if (fraccionamiento == null)
            {
                return NotFound("El usuario no existe");
            }

            fraccionamiento.estatus = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // DELETE: api/Fraccionamientos/Delete
        [HttpDelete("[action]")]
        public async Task<ActionResult<FraccionamientosViewModel>> Delete([FromBody] FraccionamientosViewModel model)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var fraccionamiento = await _context.Fraccionamientos.FirstOrDefaultAsync(f => f.id_fracc == model.id_fracc);

            if (fraccionamiento == null)
            {
                return NotFound("El fraccionamiento no existe");
            }

            _context.Fraccionamientos.Remove(fraccionamiento);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok("El fraccionamiento se ha eliminado exitosamente");
        }
    }
}
