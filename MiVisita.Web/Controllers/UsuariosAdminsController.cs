﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MiVisita.Datos;
using MiVisita.Entidades.usuarios;
using MiVisita.Web.models.usuarios.usuariosAdmin;

namespace MiVisita.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosAdminsController : ControllerBase
    {
        private readonly DbContextMiVisita _context;
        private readonly IConfiguration _config;

        public UsuariosAdminsController(DbContextMiVisita context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }

        // GET: api/UsuariosAdmins/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<UsuariosAdminViewModel>> Listar()
        {
            var usuario = await _context.UsuariosAdmin.ToListAsync();

            return usuario.Select(u => new UsuariosAdminViewModel
            {
                id_usuario = u.id_usuario,
                id_fracc = u.id_fracc,
                rol_usuario = u.rol_usuario,
                nombre = u.nombre,
                apellido_paterno = u.apellido_paterno,
                apellido_materno = u.apellido_materno,
                password_hash = u.password_hash,
                telefono = u.telefono,
                email = u.email,
                direccion = u.direccion
            });
        }

        // GET: api/UsuariosAdmins/getUsuario
        [HttpGet("[action]")]
        public async Task<ActionResult<UsuariosAdminViewModel>> getUsuario([FromBody] UsuariosAdminViewModel model)
        {
            var usuario = await _context.UsuariosAdmin.FirstOrDefaultAsync(u => u.id_usuario == model.id_usuario);

            if(usuario == null)
            {
                return NotFound("El usuario no existe");
            }

            return new UsuariosAdminViewModel
            {
                id_usuario = usuario.id_usuario,
                id_fracc = usuario.id_fracc,
                rol_usuario = usuario.rol_usuario,
                nombre = usuario.nombre,
                apellido_paterno = usuario.apellido_paterno,
                apellido_materno = usuario.apellido_materno,
                password_hash = usuario.password_hash,
                telefono = usuario.telefono,
                email = usuario.email,
                direccion = usuario.direccion
            };
        }

        // POST: api/UsuariosAdmins/Crear
        [HttpPost("[action]")]
        public async Task<ActionResult> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            string email = model.email.ToLower();

            if( await _context.UsuariosAdmin.AnyAsync( u => u.email == email))
            {
                return BadRequest("Este correo ya está registrado, por favor intente con otro.");
            }

            CrearPasswordHash(model.password, out byte[] password_hash, out byte[] password_salt);

            UsuariosAdmin usuarioAdmin = new UsuariosAdmin
            {
                id_fracc = model.id_fracc,
                rol_usuario = model.rol_usuario,
                nombre = model.nombre,
                apellido_paterno = model.apellido_paterno,
                apellido_materno = model.apellido_materno,
                password_hash = password_hash,
                password_salt = password_salt,
                telefono = model.telefono,
                email = model.email,
                direccion = model.direccion,
                estatus = true
            };

            _context.Add(usuarioAdmin);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch( Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // PUT: api/UsuariosAdmins/Actualizar
        [HttpPut("[action]")]
        public async Task<ActionResult<ActualizarViewModel>> Actualizar([FromBody] ActualizarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(model.id_usuario <= 0)
            {
                return BadRequest();
            }

            var usuarioAdmin = await _context.UsuariosAdmin.FirstOrDefaultAsync(u => u.id_usuario == model.id_usuario);

            if( usuarioAdmin == null)
            {
                return NotFound("El usuario no existe");
            }

            usuarioAdmin.id_fracc = model.id_fracc;
            usuarioAdmin.rol_usuario = model.rol_usuario;
            usuarioAdmin.nombre = model.nombre;
            usuarioAdmin.apellido_paterno = model.apellido_paterno;
            usuarioAdmin.apellido_materno = model.apellido_materno;
            usuarioAdmin.telefono = model.telefono;
            usuarioAdmin.email = model.email;
            usuarioAdmin.direccion = model.direccion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // PUT: api/UsuariosAdmins/ActualizarPassword
        [HttpPut("[action]")]
        public async Task<ActionResult<ActualizarPasswordViewModel>> ActualizarPassword([FromBody] ActualizarPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(model.id_usuario <= 0)
            {
                return BadRequest();
            }

            CrearPasswordHash(model.password, out byte[] password_hash, out byte[] password_salt);

           var usuarioAdmin = await _context.UsuariosAdmin.FirstOrDefaultAsync(u => u.id_usuario == model.id_usuario);

            if (usuarioAdmin == null)
            {
                return NotFound("El usuario no existe");
            }

            usuarioAdmin.password_hash = password_hash;
            usuarioAdmin.password_salt = password_salt;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // PUT: api/UsuariosAdmins/Activar
        [HttpPut("[action]")]
        public async Task<ActionResult<ActivarViewModel>> Activar([FromBody] ActivarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(model.id_usuario <= 0)
            {
                return BadRequest();
            }

            var UsuarioAdmin = await _context.UsuariosAdmin.FirstOrDefaultAsync(u => u.id_usuario == model.id_usuario);

            if(UsuarioAdmin == null)
            {
                return NotFound("El usuario no existe");
            }

            UsuarioAdmin.estatus = true;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // PUT: api/UsuariosAdmins/Desactivar
        [HttpPut("[action]")]
        public async Task<ActionResult<ActivarViewModel>> Desactivar([FromBody] ActivarViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (model.id_usuario <= 0)
            {
                return BadRequest();
            }

            var UsuarioAdmin = await _context.UsuariosAdmin.FirstOrDefaultAsync(u => u.id_usuario == model.id_usuario);

            if (UsuarioAdmin == null)
            {
                return NotFound("El usuario no existe");
            }

            UsuarioAdmin.estatus = false;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // POST: api/UsuariosAdmins/Login
        [HttpPost("[action]")]
        public async Task<ActionResult> Login([FromBody] LoginViewModel model)
        {

            var email = model.email.ToLower();

            var usuario = await _context.UsuariosAdmin.Where(u => u.estatus == true).FirstOrDefaultAsync(u => u.email == email);

            if (usuario == null)
            {
                return NotFound("EL usuario no está registrado.");
            }

            var rol = await _context.RolesUsuarios.FirstOrDefaultAsync(r => r.id_rol == usuario.rol_usuario);

            if(!VerificarPassword(model.password, usuario.password_hash, usuario.password_salt))
            {
                return NotFound("La contraseña no conicide con el usuario");
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, usuario.id_usuario.ToString()),
                new Claim(ClaimTypes.Email, usuario.email),
                new Claim(ClaimTypes.Role, rol.nombre),
                new Claim("id_usuario", usuario.id_usuario.ToString()),
                new Claim("rol", rol.nombre),
                new Claim("nombre", usuario.nombre)
            };

            var RefreshToken = GenerateRefreshToken();

            usuario.refresh_token = RefreshToken;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok(
                    new { jwt = GenerateToken(claims), refreshToken = RefreshToken }
                );
        }

        /// POST: api/UsuariosAdmins/Logout
        [HttpPost("[action]")]
        public async Task<ActionResult> Logout([FromBody] LogoutViewModel model)
        {
            var usuario = await _context.UsuariosAdmin.Where(u => u.estatus == true).FirstOrDefaultAsync(u => u.id_usuario == model.id_usuario);

            if (usuario == null)
            {
                return NotFound("EL usuario no está registrado.");
            }

            if (usuario.refresh_token != model.refreshToken)
            {
                return BadRequest();
            }

            usuario.refresh_token = "";
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // POST: api/UsuariosAdmins/RefreshToken
        [HttpPost("[action]")]
        public async Task<ActionResult> RefreshToken([FromBody] LogoutViewModel model)
        {
            var usuario = await _context.UsuariosAdmin.Where(u => u.estatus == true).FirstOrDefaultAsync(u => u.id_usuario == model.id_usuario);

            if (usuario == null)
            {
                return NotFound("EL usuario no está registrado.");
            }

            if (usuario.refresh_token != model.refreshToken)
            {
                return Unauthorized();
            }

            var rol = await _context.RolesUsuarios.FirstOrDefaultAsync(r => r.id_rol == usuario.rol_usuario);

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, usuario.id_usuario.ToString()),
                new Claim(ClaimTypes.Email, usuario.email),
                new Claim(ClaimTypes.Role, rol.nombre),
                new Claim("id_usuario", usuario.id_usuario.ToString()),
                new Claim("rol", rol.nombre),
                new Claim("nombre", usuario.nombre)
            };

            return Ok(
                new { jwt = GenerateToken(claims) }
                );
        }

        private string GenerateToken(List<Claim> claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                _config["Jwt:Issuer"],
                _config["Jwt:Issuer"],
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: creds,
                claims: claims);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private string GenerateRefreshToken()
        {
            var randomNumber = new byte[32];
            using (var rng = RandomNumberGenerator.Create())
            {
                rng.GetBytes(randomNumber);
                return Convert.ToBase64String(randomNumber);
            }
        }

        private bool VerificarPassword(string password, byte[] password_hash, byte[] password_salt)
        {
            using( var hmac = new HMACSHA512(password_salt))
            {
                var password_hash_nuevo = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                return new ReadOnlySpan<byte>(password_hash).SequenceEqual(new ReadOnlySpan<byte>(password_hash_nuevo));
            }
        }

        private void CrearPasswordHash(string password, out byte[] password_hash, out byte[] password_salt)
        {
            using (var hmac = new HMACSHA512())
            {
                password_salt = hmac.Key;
                password_hash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
    }
}
