﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MiVisita.Datos;
using MiVisita.Entidades.usuarios;
using MiVisita.Web.models.usuarios.rolesUsuarios;

namespace MiVisita.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RolesUsuariosController : ControllerBase
    {
        private readonly DbContextMiVisita _context;

        public RolesUsuariosController(DbContextMiVisita context)
        {
            _context = context;
        }

        // GET: api/RolesUsuarios/Listar
        [HttpGet("[action]")]
        public async Task<IEnumerable<RolesUsuariosViewModel>> Listar()
        {
            var Roles = await _context.RolesUsuarios.ToListAsync();

            return Roles.Select(r => new RolesUsuariosViewModel
            {
                id_rol = r.id_rol,
                id_fracc = r.id_fracc,
                nombre = r.nombre,
                descripcion = r.descripcion
            });
        }

        // GET: api/RolesUsuarios/GetRol/5
        [HttpGet("[action]/{id}")]
        public async Task<ActionResult<RolesUsuarios>> GetRol(int id)
        {
            if (id <= 0)
            {
                return BadRequest();
            }

            var Rol = await _context.RolesUsuarios.FirstOrDefaultAsync(r => r.id_rol == id);

            if (Rol == null)
            {
                return NotFound("El rol no existe,");
            }

            return Rol;

        }

        // PUT: api/RolesUsuarios/Actualizar
        [HttpPut("[action]")]
        public async Task<ActionResult<RolesUsuariosViewModel>> Actualizar([FromBody] RolesUsuariosViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(model.id_rol <= 0)
            {
                return BadRequest();
            }

            var Rol = await _context.RolesUsuarios.FirstOrDefaultAsync(r => r.id_rol == model.id_rol);

            if(Rol == null)
            {
                return NotFound("El rol no existe.");
            }

            Rol.id_fracc = model.id_fracc;
            Rol.nombre = model.nombre;
            Rol.descripcion = model.descripcion;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        // POST: api/RolesUsuarios/Crear
        [HttpPost("[action]")]
        public async Task<ActionResult<CrearViewModel>> Crear([FromBody] CrearViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            string nombre = model.nombre;

            if (await _context.RolesUsuarios.AnyAsync(u => u.nombre == nombre))
            {
                return BadRequest("Este rol ya existe, por favor elija otro.");
            }

            RolesUsuarios Rol = new RolesUsuarios
            {
                id_fracc = model.id_fracc,
                nombre = model.nombre,
                descripcion = model.descripcion
            };

            _context.Add(Rol);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();
        }

        private bool RolesUsuariosExists(int id)
        {
            return _context.RolesUsuarios.Any(e => e.id_rol == id);
        }
    }
}
